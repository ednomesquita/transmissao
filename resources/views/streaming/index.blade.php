@extends('layouts.master')

@section('title')
    {{ config('app.name') }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card-header">
                    <h1 class="h3">Transmissão Oi</h1>
                </div>

                <div class="wrapper">
                    <div class="card-deck">
                        <div class="card video-streaming  col-sm-12">
                            <div class="card-body">
                                <h5 class="card-title">Vídeo</h5>
                            </div>
                            <div class="card-img-top" id="player">Loading the player...</div>
                        </div>

                        <div class="card slide-streaming  col-sm-12">
                            <div class="card-body">
                                <h5 class="card-title">Slides</h5>
                            </div>
                            <div class="card-img-top" id="player2">Loading the player...</div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection


@section('footer')

<footer class="page-footer font-small unique-color-dark pt-4 pb-4 mt-lg-auto">
    <div class="container">
        <p>Em caso de congelamento ou travamento da imagem da transmissão, a orientação é que realize a atualização da página, pressionando a tecla "F5" do seu computador;</p>
        <p>As perguntas para o presidente e diretores N1 dever o ser enviadas por Whatsapp para: (21) 98991-1111;</p>
        <p>Em caso de problema com login e senha, entre em contato com a Atmo (11) 2990-1280, em caso de problemas de rede, ligue para o Helpdesk de TI (0800 282 0031) opção 00;</p>
        <p>Você pode acompanhar o evento em uma sala de vídeo espalhada em todo o Brasil. Confira a lista na Interativa;</p>
        <p>Use fone de ouvido.</p>
    </div>
    <!-- Footer Elements -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
        <a href="http://atmodigital.com.br"> atmodigital.com.br</a>
    </div>
</footer>
    <!-- Copyright -->
@endsection

@section('script')
    <script>
        const player =  jwplayer('player').setup({
            "title": "Transmissão",
            "file": "https://rtmp.cdn.upx.net.br/b096_2/myStream.sdp1/playlist.m3u8",
            "label": "0",
            "volume": "20",
            "height": "auto",
            "width": "100%",
            "autostart": "viewable",
            "primary": "html5",
            "hlshtml": true,
            "autostart": true
        });

        const player2 =  jwplayer('player2').setup({
            "title": "Slides",
            "file": "https://rtmp.cdn.upx.net.br/b096_1/myStream.sdp1/playlist.m3u8",
            "height": "auto",
            "width": "100%",
            "autostart": "viewable",
            "controls": true
        });
    </script>
@endsection

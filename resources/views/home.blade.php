@extends('layouts.master')

@section('title')
    {{ config('app.name') }}
@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card-header">
                <h1 class="h3">Transmissão Oi</h1>
            </div>

            <div class="wrapper">
                <div class="video-streaming col-md-6 col-sm-12">
                    <div id="player">Loading the player...</div>
                </div>

                <div class="slide-streaming col-md-6 col-sm-12">
                    <div id="player2">Loading the player...</div>
                </div>
            </div>

            <script>
                const player =  jwplayer('player').setup({
                    "title": "Transmissão",
                    "file": "https://rtmp.cdn.upx.net.br/b096_2/myStream.sdp1/playlist.m3u8",
                    "label": "0",                                   
                    "volume": "20",
                    "height": "auto",
                    "width": "100%",
                    "autostart": "viewable",
                    "primary": "html5",
                    "hlshtml": true,
                    "autostart": true                                                         
                });

                const player2 =  jwplayer('player2').setup({
                    "title": "Slides",
                    "file": "https://rtmp.cdn.upx.net.br/b096_1/myStream.sdp1/playlist.m3u8",
                    "height": "auto",
                    "width": "100%",
                    "autostart": "viewable",
                    "controls": true
                });                
            </script>


            <script>
                setInterval(function(){
                    $.ajax({
                        url: "/sessions/store",
                        method: "GET"
                    }).done(function(data) {
                        console.log(data);
                    });
                }, 60000);
            </script>
        </div>
    </div>
</div>
@endsection

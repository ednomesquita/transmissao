<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 24px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        {{--<a href="{{ route('register') }}">Register</a>--}}
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <h1 class="col-md-12 col-sm-12">
                        Transmissão
                    </h1>
                </div>

                <div class="col-md-12">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 528.1" enable-background="new 0 0 612 528.1"><linearGradient id="purple-gradient" gradientUnits="userSpaceOnUse" x1="34.582" y1="264.026" x2="568.693" y2="264.026"><stop offset="0" style="stop-color:#E9278B"></stop><stop offset=".062" style="stop-color:#D528AB"></stop><stop offset=".136" style="stop-color:#C129C9"></stop><stop offset=".214" style="stop-color:#B229E1"></stop><stop offset=".297" style="stop-color:#A72AF2"></stop><stop offset=".387" style="stop-color:#A12AFC"></stop><stop offset=".5" style="stop-color:#9F2AFF"></stop><stop offset=".598" style="stop-color:#9C2DFF"></stop><stop offset=".676" style="stop-color:#9237FF"></stop><stop offset=".748" style="stop-color:#8248FF"></stop><stop offset=".816" style="stop-color:#6A60FF"></stop><stop offset=".881" style="stop-color:#4C7FFF"></stop><stop offset=".943" style="stop-color:#28A4FF"></stop><stop offset="1" style="stop-color:#00CDFF"></stop></linearGradient><path fill="url(#purple-gradient)" d="M568.7 310.3c0 83.3-109 186.3-251.2 186.3-155.6 0-283-90.6-283-241.1 0-103.1 69.7-224.1 202.1-224.1 117.9 0 161.7 82.8 219 120.8 63.9 42.3 113.1 80 113.1 158.1z"></path><path fill="#FFFFFF" d="M349.5 215.4c11.9 0 21.5-9.6 21.5-21.5 0-11.8-9.6-21.5-21.5-21.5-11.8 0-21.4 9.6-21.4 21.5-.1 11.9 9.5 21.5 21.4 21.5zm.3 15.9c-12.1 0-21.9 28.7-21.9 64.1 0 35.4 9.8 64.1 21.9 64.1 12.1 0 21.9-28.7 21.9-64.1-.1-35.4-9.9-64.1-21.9-64.1zm-97.5-.1c-36 0-59.9 28.5-59.9 64.1 0 35.7 23.9 64.1 59.9 64.1 35.9 0 59.7-28.4 59.7-64.1 0-35.6-23.9-64.1-59.7-64.1zm0 98.1c-16.5 0-25.6-16.9-25.6-34.1 0-17.2 9.1-33.6 25.6-33.6 16.4 0 25.5 16.5 25.5 33.6s-9.1 34.1-25.5 34.1z"></path></svg>
                </div>


            </div>
        </div>
    </body>
</html>

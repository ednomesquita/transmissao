@extends('layouts.master')

@section('title')
    Dashboard - Usuários
@endsection


@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            @include('users.sidebar')
            <div class="col-md-10">
                <div class="card-header">
                    <h1 class="h3">Usuários</h1>
                </div>
                {{--<div class="">--}}
                {{--<div class="card-header">Dashboard</div>--}}
                {{--<div class="card-body">--}}
                {{--@if (session('status'))--}}
                {{--<div class="alert alert-success" role="alert">--}}
                {{--{{ session('status') }}--}}
                {{--</div>--}}
                {{--@endif--}}
                {{--</div>--}}
                {{--</div>--}}
                <a href=" {{ url('/users/create') }}" class="btn btn-primary pull-right app-button"  role="button" aria-pressed="true">Novo</a>
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">usuário</th>
                        <th scope="col">email</th>
                        <th scope="col">ações</th>
                        <th scope="col">{{-- ##--}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td> {{ $user->email }}</td>
                            <td><a href="{{ url("users/$user->id/edit") }}"><i class="fa fa-pencil" aria-hidden="true" title="editar"></i></a></td>
                            <td>
                                <form action="/users/{{ $user->id }}/destroy" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit"><i class="fa fa-trash-o" aria-hidden="true" title="excluir"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>     
        </div>    

        <div class="row justify-content-center">
            {{ $users->links() }}        
        </div>    
    </div>
@endsection

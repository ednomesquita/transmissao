<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="{{ url('users/index') }}">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    Usuários
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ url('transmissao') }}">
                <i class="fa fa-video-camera" aria-hidden="true"></i>
                      Transmissão
                </a>
            </li>                        
        </ul>
    </div>
</nav>
<?php

namespace App\Http\Controllers;

use App\Streaming;

class StreamingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display video streaming
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sources = Streaming::pluck('source');

        return view('streaming.index', [
            'video' => $sources[0],
            'slide' => $sources[1]
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\UserActivity;

class UserActivityController extends Controller
{

    /**
     * Store a newly created resource in storage.
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        $register = UserActivity::create([
            'last_seen' => Carbon::now(
                new \DateTimeZone('America/Sao_Paulo'))
                            ->format("Y-m-d H:i:s"),
            'user_id' => auth()->user()->id,
            'session_id' => session()->getId(),
            'visitor' => request()->ip(),
            'user_agent' => request()->header('user-agent')
        ]);

        return response()->json($register, 200);
    }


}

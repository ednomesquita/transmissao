<?php

## Auth facades
Auth::routes();

## Transmissao
Route::get('/', 'StreamingController@index');
Route::get('/transmissao', 'StreamingController@index')->name('transmissao');
Route::get('/home', 'StreamingController@index')->name('transmissao');

## User
Route::get('/users/index', 'UserController@index');
Route::get('/users/{user}/edit', 'UserController@edit');
Route::get('/users/create', 'UserController@create');
Route::post('/users/store', 'UserController@store');
Route::put('/users/{user}/update', 'UserController@update');
Route::delete('/users/{user}/destroy', 'UserController@destroy');

## Heartbeat
Route::get('/sessions/store', 'UserActivityController@store');